package app.services.builders

import app.controllers.dto.DeckDto
import app.converters.DeckConverter
import app.repository.storage.entities.Card
import app.repository.storage.entities.CardValue
import app.repository.storage.entities.Deck
import app.repository.storage.entities.Suit
import org.springframework.stereotype.Service
import java.util.*
import kotlin.random.Random

@Service
class DeckBuilderService(val deckConverter: DeckConverter) {
    
    //TODO: think of ways for building deck around jokers
    fun buildDefault52Deck(): DeckDto {
        val resultDeck = Deck(Stack())
        for (value in CardValue.values()) {
            for (suit in Suit.values()) {
                resultDeck.cards.add(Card(value, suit))
            }
        }
        return deckConverter.convert(resultDeck.apply { cards.shuffle(Random.Default) })
    }
}