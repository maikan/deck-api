package app.services.dao.dev

import app.exceptions.IdOutOfBoundException
import app.exceptions.NoSuchObjectException
import app.repository.storage.DeckRepository
import app.repository.storage.entities.Deck
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service("storage")
@Profile("dev") // spring.profiles.active=dev
object DevDeckDao : DeckRepository<Deck, BigDecimal> {

    private var deckStorage: HashMap<BigDecimal, Deck> = HashMap()
    private var deckCounter: BigDecimal = BigDecimal(0)

    override fun put(obj: Deck): BigDecimal {
        deckStorage[++deckCounter] = obj
        return deckCounter
    }

    override fun update(id: BigDecimal, obj: Deck) {
        checkId(id)
        validateDeckValue(id)
        deckStorage[id] = obj
    }

    override fun get(id: BigDecimal): Deck {
        checkId(id)
        return validateDeckValue(id)
    }

    override fun delete(id: BigDecimal) {
        checkId(id)
        validateDeckValue(id)
        deckStorage.remove(id)
    }

    private fun checkId(id: BigDecimal) {
        if (id > deckCounter || id <= BigDecimal.ZERO)
            throw IdOutOfBoundException()
    }
    
    private fun validateDeckValue(id: BigDecimal): Deck {
        return deckStorage[id] ?: throw NoSuchObjectException()
    }
}