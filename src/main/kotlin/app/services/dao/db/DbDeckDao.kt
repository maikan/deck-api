package app.services.dao.db

import app.repository.storage.DeckRepository
import app.repository.storage.entities.Deck
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service("storage")
@Profile("default")
object DbDeckDao: DeckRepository<Deck, BigDecimal> {
    
    override fun put(obj: Deck): BigDecimal {
        TODO("Not yet implemented")
    }

    override fun update(id: BigDecimal, obj: Deck) {
        TODO("Not yet implemented")
    }

    override fun get(id: BigDecimal): Deck {
        TODO("Not yet implemented")
    }

    override fun delete(id: BigDecimal) {
        TODO("Not yet implemented")
    }
}