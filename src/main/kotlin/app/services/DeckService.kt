package app.services

import app.controllers.dto.DeckDto
import app.converters.DeckConverter
import app.repository.storage.DeckRepository
import app.repository.storage.entities.Deck
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class DeckService(
    val deckRepository: DeckRepository<Deck, BigDecimal>,
    val deckConverter: DeckConverter
) {

    fun put(deck: DeckDto): BigDecimal =
        deckRepository.put(deckConverter.convert(deck))

    fun update(id: BigDecimal, deck: DeckDto) =
        deckRepository.update(id, deckConverter.convert(deck))

    fun get(id: BigDecimal): DeckDto =
        deckConverter.convert(deckRepository.get(id))

    fun delete(id: BigDecimal) =
        deckRepository.delete(id)
}