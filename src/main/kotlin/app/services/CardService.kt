package app.services

import app.controllers.dto.CardDto
import app.converters.CardConverter
import app.repository.storage.DeckRepository
import app.repository.storage.entities.Deck
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service
class CardService(
    val deckRepository: DeckRepository<Deck, BigDecimal>,
    val cardConverter: CardConverter
) {

    fun getCardFromTop(id: BigDecimal): CardDto =
        cardConverter.convert(deckRepository.get(id).drawCardFromTop())
}