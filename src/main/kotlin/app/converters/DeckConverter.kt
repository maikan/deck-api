package app.converters

import app.controllers.dto.CardDto
import app.controllers.dto.DeckDto
import app.repository.storage.entities.Card
import app.repository.storage.entities.Deck
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component
import java.util.*

@Component
class DeckConverter(val cardConverter: CardConverter) : Converter<DeckDto, Deck> {

    override fun convert(source: DeckDto): Deck = Deck(
        source.cards.mapTo(
            Stack<Card>(),
            cardConverter::convert
        )
    )

    fun convert(source: Deck): DeckDto =
        DeckDto(
            source.cards.mapTo(
                Stack<CardDto>(),
                cardConverter::convert
            )
        )
}