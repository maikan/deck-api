package app.converters

import app.controllers.dto.CardDto
import app.repository.storage.entities.Card
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component

@Component
class CardConverter : Converter<CardDto, Card> {

    override fun convert(source: CardDto): Card = with(source) {
        Card(
            suit = suit,
            value = value
        )
    }

    fun convert(source: Card): CardDto = with(source) {
        CardDto(
            suit = suit,
            value = value
        )
    }
}