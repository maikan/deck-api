package app.repository.storage.entities

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotNull

//TODO: custom comparator (value > suit/ suit > value)
@Validated
class Card(
    @JsonProperty("value")
    @field:NotNull
    val value: CardValue,
    @JsonProperty("suit")
    @field:NotNull
    val suit: Suit
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Card

        if (value != other.value) return false
        if (suit != other.suit) return false

        return true
    }

    override fun hashCode(): Int {
        var result = value.hashCode()
        result = 31 * result + suit.hashCode()
        return result
    }
}