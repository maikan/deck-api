package app.repository.storage.entities

//TODO: wildcard
enum class Suit {

    CLUBS,
    DIAMONDS,
    HEARTS,
    SPADES
}
