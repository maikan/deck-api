package app.repository.storage.entities

import org.springframework.validation.annotation.Validated
import java.util.*

@Validated
class Deck(val cards: Stack<Card>) {

    fun drawCardFromTop(): Card =
        if (cards.isNotEmpty()) cards.pop() else throw InternalError("Deck is empty")
}