package app.repository.storage

import org.springframework.stereotype.Repository

//T - subject, N - ID number
@Repository
interface DeckRepository<T, N> {
    
    fun put(obj: T): N
    
    fun update(id: N, obj: T)
    
    fun get(id: N): T
    
    fun delete(id: N)
}