package app.controllers

import app.services.builders.DeckBuilderService
import app.controllers.DeckGeneratorController.Companion.ROOT_URI
import app.controllers.dto.DeckDto
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(ROOT_URI)
class DeckGeneratorController(val deckBuilder: DeckBuilderService) {
    
    @GetMapping(DEFAULT_DECK)
    fun defaultDeck(): DeckDto = 
        deckBuilder.buildDefault52Deck()
    
    companion object {
        const val ROOT_URI = "/build"
        const val DEFAULT_DECK = "/defaultDeck"
    }
}