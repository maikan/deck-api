package app.controllers.dto

import app.repository.storage.entities.CardValue
import app.repository.storage.entities.Suit
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotNull

data class CardDto(
    @JsonProperty("value")
    @field:NotNull
    val value: CardValue,
    @JsonProperty("suit")
    @field:NotNull
    val suit: Suit
)