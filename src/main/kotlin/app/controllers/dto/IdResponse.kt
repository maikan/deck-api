package app.controllers.dto

data class IdResponse(val id: String)