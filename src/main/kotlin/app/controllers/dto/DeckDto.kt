package app.controllers.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*
import javax.validation.constraints.NotEmpty

data class DeckDto(@JsonProperty("cards")
                   @field:NotEmpty 
                   val cards: Stack<CardDto>,
                   val cardsCount: Int = cards.size)