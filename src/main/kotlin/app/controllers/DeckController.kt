package app.controllers

import app.controllers.DeckController.Companion.ROOT_URI
import app.controllers.dto.CardDto
import app.controllers.dto.DeckDto
import app.controllers.dto.IdResponse
import app.repository.storage.entities.Card
import app.repository.storage.entities.Deck
import app.services.CardService
import app.services.DeckService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.math.BigDecimal

//TODO: store generated decks somewhere else
// assigning deck to certain user?
@RestController
@RequestMapping(ROOT_URI)
class DeckController(val cardService: CardService,
                     val deckService: DeckService) {

    @GetMapping(ID_URI)
    fun getDeck(@PathVariable deckId: BigDecimal): DeckDto =
        deckService.get(deckId)

    @GetMapping(GET_CARD)
    fun getCardFromDeck(@PathVariable deckId: BigDecimal): CardDto =
        cardService.getCardFromTop(deckId)

    @PutMapping
    fun addDeck(@RequestBody deck: DeckDto): IdResponse =
        IdResponse(deckService.put(deck).toPlainString())

    @PostMapping(ID_URI)
    @ResponseBody
    fun modifyDeck(
        @PathVariable deckId: BigDecimal,
        @RequestBody deck: DeckDto
    ) {
        deckService.update(deckId, deck)
        ResponseEntity<Nothing>(HttpStatus.OK)
    }

    @DeleteMapping(ID_URI)
    @ResponseBody
    fun deleteDeck(@PathVariable deckId: BigDecimal) {
        deckService.delete(deckId)
        ResponseEntity<Nothing>(HttpStatus.OK)
    }

    companion object {
        const val ROOT_URI = "/deck"
        const val ID_URI = "/{deckId}"
        const val GET_CARD = "$ID_URI/getCard"
    }
}