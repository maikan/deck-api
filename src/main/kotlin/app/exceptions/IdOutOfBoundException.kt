package app.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class IdOutOfBoundException(message: String = "Index Out Of Bounds"): Exception(message)