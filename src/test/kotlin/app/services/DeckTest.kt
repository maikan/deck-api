package app.services

import app.repository.storage.entities.Card
import app.repository.storage.entities.CardValue.*
import app.repository.storage.entities.Deck
import app.repository.storage.entities.Suit.*
import org.junit.jupiter.api.Assertions.assertAll
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import java.util.*

@SpringBootTest
class DeckTest {

    @Test
    fun drawCardFromTop() {
        assertAll(
            { assertEquals(Card(KING, CLUBS), testDeck.drawCardFromTop()) },
            { assertEquals(Card(TWO, SPADES), testDeck.drawCardFromTop()) },
            { assertEquals(1, testDeck.cards.size) }
        )
    }
    
    companion object {
        val testDeck = Deck(Stack()).apply { 
            this.cards.addAll(listOf(
                Card(EIGHT, HEARTS),
                Card(TWO, SPADES),
                Card(KING, CLUBS)
            ))
        }
    }
}