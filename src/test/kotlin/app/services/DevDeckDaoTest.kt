package app.services

import app.repository.storage.entities.Card
import app.repository.storage.entities.CardValue
import app.repository.storage.entities.Deck
import app.repository.storage.entities.Suit
import app.exceptions.IdOutOfBoundException
import app.exceptions.NoSuchObjectException
import app.repository.storage.DeckRepository
import org.junit.jupiter.api.Assertions.assertAll
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import java.math.BigDecimal
import java.util.*

@ActiveProfiles("dev")
@SpringBootTest
class DevDeckDaoTest {

    @Autowired
    lateinit var deckRepository: DeckRepository<Deck, BigDecimal>

    lateinit var lastDeckId: BigDecimal

    @BeforeEach
    fun setupForEach() {
        lastDeckId = deckRepository.put(testDeck)
    }

    @Test
    fun successfullyAddDeck() {
        assertEquals(testDeck, deckRepository.get(lastDeckId))
    }

    @Test
    fun successfullyModifyDeck() {
        val modifiedTestDeck = (Deck(testDeck.cards))
            .apply { cards.add(Card(CardValue.SIX, Suit.CLUBS)) }
        deckRepository.update(lastDeckId, modifiedTestDeck)

        assertEquals(modifiedTestDeck, deckRepository.get(lastDeckId))
    }

    @Test
    fun wrongIdWhenModifyingDeck() {
        assertAll(
            { assertThrows<IdOutOfBoundException> { deckRepository.update(BigDecimal(-1), testDeck) } },
            { assertThrows<IdOutOfBoundException> { deckRepository.update(BigDecimal(0), testDeck) } },
            { assertThrows<IdOutOfBoundException> { deckRepository.update(lastDeckId + BigDecimal.ONE, testDeck) } }
        )
    }

    @Test
    fun nonexistentDeckModifying() {
        deckRepository.delete(BigDecimal.ONE)
        assertThrows<NoSuchObjectException> { deckRepository.update(BigDecimal.ONE, testDeck) }
    }

    @Test
    fun successfullyRemoveDeck() {
        deckRepository.delete(lastDeckId)

        assertThrows<NoSuchObjectException> { deckRepository.get(lastDeckId) }
    }

    /* TODO: - Null safety (put, update), or probably would be wiser to move it to controller tests
             -
    */

    companion object {
        val testDeck = Deck(Stack()).apply {
            this.cards.addAll(
                listOf(
                    Card(CardValue.EIGHT, Suit.HEARTS),
                    Card(CardValue.TWO, Suit.SPADES),
                    Card(CardValue.KING, Suit.CLUBS)
                )
            )
        }
    }
}