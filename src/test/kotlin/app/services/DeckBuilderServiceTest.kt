package app.services

import app.services.builders.DeckBuilderService
import org.junit.jupiter.api.Assertions.assertAll
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class DeckBuilderServiceTest {

    @Autowired
    lateinit var deckBuilder: DeckBuilderService

    @Test
    fun buildDefault52Deck() {
        val deck = DeckBuilderService.buildDefault52Deck()

        assertAll(
            { assertEquals(52, deck.cards.size) },
            { assertEquals(52, deck.cards.distinct().size) }
        )
    }
}